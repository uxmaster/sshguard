Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sshguard
Upstream-Contact: Mij <mij@bitchx.it>
Source: https://bitbucket.org/sshguard/sshguard/src

Files: *
Copyright: 2007-2011 Mij <mij@bitchx.it>
License: ISC

Files: debian/*
Copyright: 2009-2019 Julián Moreno Patiño <julian@debian.org>
 2007 Ernesto Nadir Crespo Avila <ecrespo@debianvenezuela.org>
License: ISC

Files: src/*
Copyright: 2007-2011, Mij <mij@sshguard.net>
License: ISC

Files: src/parser/attack_parser.c
  src/parser/attack_parser.h
Copyright: 1984, 1989, 1990, 2000-2015, Free Software Foundation, Inc.
License: GPL-3+

Files: src/blocker/fnv.h
  src/blocker/hash_32a.c
Copyright: 2009 Landon Curt Noll <chongo@mellis.com>
License: public-domain
 Please do not copyright this code.  This code is in the public domain.
 .
 LANDON CURT NOLL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
 EVENT SHALL LANDON CURT NOLL BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007, or (at
 your option) any later version.
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
